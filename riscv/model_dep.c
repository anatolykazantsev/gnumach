#include <device/cons.h>

#include <mach/machine.h>
#include <mach/machine/multiboot.h>

#include <kern/printf.h>
#include <kern/startup.h>

#include <riscv/include/sbi.h>

/* A copy of the multiboot info structure passed by the boot loader.  */
struct multiboot_info boot_info;

enum {
    /* UART Registers */
    UART_REG_TXFIFO = 0,
};

static volatile int *uart = (int *)(void *)0x10010000;

void uart_putchar(char ch)
{
    while (uart[UART_REG_TXFIFO] < 0);
    uart[UART_REG_TXFIFO] = ch & 0xff;
}

void halt()
{
    while(1); 
}

extern char version[];

/*
 * C boot entrypoint - called by boot_entry in boothdr.S.
 * Running in flat mode, but without paging yet.
 */
void c_boot_entry(/*vm_offset_t bi*/)
{
    romputc = uart_putchar;

    /* Stash the boot_image_info pointer.  */
    //boot_info = *(typeof(boot_info)*)phystokv(bi);
    boot_info = (struct multiboot_info) {
        .flags = 0,
        .mem_lower = 0,
        .mem_upper = 0,
        .boot_device = "",
        .cmdline = 0,
        .mods_count = 0,
        .mods_addr = 0,
        .syms = {
            .e = {
                .num = 0,
                .size = 0,
                .addr = 0,
                .shndx = 0
            }
        },
        .mmap_count = 0,
        .mmap_addr = 0
    };

    /* Before we do _anything_ else, print the hello message.
       If there are no initialized console devices yet,
       it will be stored and printed at the first opportunity. */
    printf("%s", version);
    printf("\n");

    sbi_init();
    sbi_print_version();

    machine_slot[0].is_cpu = TRUE;
    machine_slot[0].running = TRUE;
    machine_slot[0].cpu_type = CPU_TYPE_RISCV;
    machine_slot[0].cpu_subtype = CPU_SUBTYPE_RISCV_64;

    /*
	 * Start the system.
	 */
	setup_main();

    //sbi_system_reset(SBI_SRST_RESET_TYPE_SHUTDOWN, SBI_SRST_RESET_REASON_NO_REASON);
    sbi_shutdown();
    //halt();
}
