#!/bin/sh

IMAGE='mach-riscv/make'

read -r -d '' CONTAINERFILE <<'EOF'
FROM docker.io/alpine:edge AS mig

ARG MIG_COMMIT=e5c3fa44fb75b23dc7100202e52f3d4366447851

COPY mig-headers/mach /tmp/include/mach
COPY mig-headers/kern /tmp/include/kern

RUN apk add --no-cache build-base gcc-riscv-none-elf autoconf automake bison flex && \
    wget http://git.savannah.gnu.org/cgit/hurd/mig.git/snapshot/mig-$MIG_COMMIT.tar.gz -O /tmp/mig-$MIG_COMMIT.tar.gz && \
    mkdir /tmp/mig && \
    tar -xf /tmp/mig-$MIG_COMMIT.tar.gz -C /tmp/mig --strip-components=1 && \
    cd /tmp/mig && \
    autoreconf -vif && \
    ./configure --prefix=/usr/local --sysconfdir=/etc --mandir=/usr/share/man --localstatedir=/var \
      --target="riscv-none-elf" \
      TARGET_CPPFLAGS="-march=rv32g -mabi=ilp32 -mcmodel=medany -I/tmp/include -D KERNEL=1 -D MACH_KERNEL=1 -D NCPUS=1 -D CPU_L1_SHIFT=6 -D STAT_TIME=1" \
      TARGET_CC="riscv-none-elf-gcc" TARGET_CFLAGS="-fno-builtin" && \
    make && \
    make install

FROM docker.io/alpine:edge

ENTRYPOINT ["make"]
WORKDIR /repo

COPY --from=mig /usr/local/bin/riscv-none-elf-mig /usr/local/bin/riscv-none-elf-mig
COPY --from=mig /usr/local/libexec/riscv-none-elf-migcom /usr/local/libexec/riscv-none-elf-migcom

RUN apk add --no-cache gcc-riscv-none-elf make perl
EOF

dir_root=$(realpath "$PWD")

build () {
  echo "$CONTAINERFILE" | podman build --no-cache -t "$IMAGE" --file - .
}

run () {
  podman \
    run \
      --userns keep-id \
      -it \
      --rm \
      --volume "$dir_root":/repo \
      "$IMAGE" $@
}

is_container_built () {
  podman image exists "$IMAGE"
}

if ! is_container_built; then
  build || exit
fi

run $@
